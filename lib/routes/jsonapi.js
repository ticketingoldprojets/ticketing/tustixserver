/**
 * ChipoJobber JSON Api
 */

exports.requestHandler = function(req, res, next) {
    var pgClient = req.user;
    var pgFunction = req.get('pgFunction');
    //console.log('pgFunction: ', pgFunction);
    //console.error('request data:', req.body);
    var q = 'SELECT * FROM '+pgFunction+(req.method==='GET'?'()':'($1)');//::json
    console.log('query: ', q);
    pgClient.query(q, req.method==='GET'?[]:[JSON.stringify(req.body)], function(err, result) {    
        if(err) {
            console.error(err);
            //console.error('request data:', req.body);
            //res.json(403, {ERRCODE: err.code, MESSAGE: err.message, HINT: err.hint, DETAIL: err.detail});
            
            var exp_err;
            
            if(err.code.slice(0,2) == 'TX') //Custom error codes
            {
                exp_err = new Error(err.message);
                exp_err.code = err.code.slice(2);
            }
            else
            {
                exp_err = new Error('Internal Server Error');
                exp_err.code = 500; //To hide database error details, no dar de comer a los hackers
            }
            
            // forward control on to the next registered error handler:
            return next(exp_err);
        }
        else
        {
            console.log('response:', result.rows);
            res.json(result.rows);
        }
    });
};
