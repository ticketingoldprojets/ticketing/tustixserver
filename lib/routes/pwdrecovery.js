/**
 * New node file
 */
var nodemailer = require("nodemailer");
var ejs = require('ejs');
var config = require('../../config.js');

exports.pwdRecoveryHandler = function(req, res) {
    var pgClient = req.user;
    pgClient.query('SELECT * FROM pwdrecovery($1)', [JSON.stringify(req.body)], function(err, result) {    
        if(err) {
            console.error(err);
            console.error('request data:', req.body);
            res.json(500, {ERRCODE: err.code, MESSAGE: err.message, HINT: err.hint, DETAIL: err.detail});
        }
        else
        {
            //SEND MAIL
            console.log('Sending mail...');
            // create reusable transport method (opens pool of SMTP connections)
            var transport = nodemailer.createTransport("SMTP", {
                service: "Gmail",
                auth: {
                    user: config.main_email,
                    pass: config.main_email_pass
                }
            });
            
            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: config.main_email, // sender address
                to: req.body.email, // list of receivers
                subject: gettext("Password recovery request on TixRealm"), // Subject line
                //text: "Hello world", // plaintext body
                html: ejs.render("doc/email/password_recovery.ejs").data(result.rows) // html body
            };
            
            transport.sendMail(mailOptions, function(error, responseStatus) {
                if(!error) {
                    console.log(responseStatus.message); // response from the server
                    console.log(responseStatus.messageId); // Message-ID value used
                }
                else
                    console.log(error);
            });

            res.json(result.rows);
        }
    });
};
