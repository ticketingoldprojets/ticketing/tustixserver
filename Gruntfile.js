module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-angular-gettext');
    grunt.loadNpmTasks('grunt-gettext');
    grunt.loadNpmTasks('grunt-i18n-abide');
    
    grunt.initConfig({
        nggettext_extract: {
            pot: {
                files: {
                    '../client/i18n/template.pot': ['../client/src/*.html']
                }
            }
        },
    
        nggettext_compile: {
            all: {
                files: {
                    '../client/src/app/translations.js': ['../client/i18n/*.po']
                }
            }
        },
        
        xgettext: {
            options: {
                functionName: "gettext",
                potFile: "i18n/template.pot"
            },

            files: {
                /**
                 * JavaScript files to scan for translatable texts.
                 *
                 * Assuming the default functionName is used, translatable
                 * messages look like this:
                 *
                 * tr("Some translatable messages")
                 * tr("You have %1 follower", "You have %1 followers")
                 *                                       .arg(numFollowers)
                 *
                 * In both cases, all string arguments inside the tr()
                 * function call are extracted as translatable messages.
                 */
                javascript: ['lib/routes/pwdrecovery.js']
            }
        },
        
        abideExtract: {
            js: {
              src: 'lib/*.js',
              dest: 'i18n/template.pot',
              options: {
                language: 'JavaScript',
              }
            }
        }
    });
};