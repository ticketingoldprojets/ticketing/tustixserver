import { Component, OnInit, Input } from '@angular/core';

@Component({
  ///moduleId: module.id,
  selector: 'app-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.css']
})
export class EventComponent implements OnInit {
  @Input() private name: string;
  @Input() private desc: string;
  @Input() private price: number;
  @Input() private image: string | null = null;

  constructor() {
  }

  ngOnInit() {
  }
}
