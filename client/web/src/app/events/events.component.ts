class Event {
  public name: string;
  public desc: string;
  public price: number;
  public image: string | null;

  public constructor({name, desc, price, image}) {
    this.name = name;
    this.desc = desc;
    this.price = price;
    this.image = image;
  }
}


import { Component } from '@angular/core';

import { Home } from '../home';

import { AppState } from '../app.service';
import { Title } from '../home/title';
import { XLarge } from '../home/x-large';

@Component({
  // The selector is what angular internally uses
  // for `document.querySelectorAll(selector)` in our index.html
  // where, in this case, selector is the string 'home'
  selector: 'app-events',  // <home></home>
  // We need to tell Angular's Dependency Injection which providers are in our app.
  providers: [
    Title
  ],
  // We need to tell Angular's compiler which directives are in our template.
  // Doing so will allow Angular to attach our behavior to an element
  directives: [
    XLarge
  ],
  // We need to tell Angular's compiler which custom pipes are in our template.
  pipes: [ ],
  // Our list of styles in our component. We may add more to compose many styles together
  styleUrls: [ 'events.component.css' ],
  // Every Angular template is first compiled by the browser before Angular runs it's compiler
  templateUrl: 'events.component.html'
})
export class EventsComponent {
  public events: Event[];

  // Set our default values (TODO: Use Redux for this)
  localState = {value: ''};

  // TypeScript public modifiers
  constructor(public appState: AppState, public title: Title) {
    this.events = [
      new Event({name: "Spectacle 1", desc: "Ici le description pour l'événement 1...", price: 1050, image: "mc-circus-dinner-show-2013-lg.jpg"}),
      new Event({name: "Spectacle 2", desc: "Ici le description pour l'événement 2...", price: 1235, image: "dinner-show.jpg"}),
      new Event({name: "Spectacle 3", desc: "Ici le description pour l'événement 3...", price: 795, image: "show.jpg"}),
      new Event({name: "Spectacle 4", desc: "Ici le description pour l'événement 4...", price: 1000, image: "TheMuppetShow.jpg"})
    ];
  }

  ngOnInit() {
    console.log('hello `EventsComponent` component');
    // this.title.getData().subscribe(data => this.data = data);

    [
      {name: "Spectacle 5", desc: "Ici le description pour l'événement 5...", price: 5555, image: "images.jpg"},
      {name: "Spectacle 6", desc: "Ici le description pour l'événement 6...", price: 6666, image: "images (1).jpg"},
      {name: "Spectacle 7", desc: "Ici le description pour l'événement 7...", price: 7777, image: "images (6).jpg"},
      {name: "Spectacle 8", desc: "Ici le description pour l'événement 8...", price: 8888, image: "images (3).jpg"},
      {name: "Spectacle 9", desc: "Ici le description pour l'événement 9...", price: 9999, image: "images (4).jpg"},
      {name: "Spectacle 10", desc: "Ici le description pour l'événement 10...", price: 1111, image: "images (5).jpg"},
      {name: "Spectacle 11", desc: "Ici le description pour l'événement 11...", price: 2222, image: "images (2).jpg"},
    ].forEach((event, index) => setTimeout(() => this.events.push(new Event(event)), index * 3000));
  }

  submitState(value) {
    console.log('submitState', value);
    this.appState.set('value', value);
    this.localState.value = '';
  }
}
