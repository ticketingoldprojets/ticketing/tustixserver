
/**
 * Module dependencies.
 */

/**
 * Read configuration file
 */
var fs = require('fs');
var passport = require('passport');

var express = require('express');
var routes = require('./lib/routes');
var https = require('https');
var path = require('path');
var config = require(__dirname + '/config.js');
var pg = require('pg');
var BasicStrategy = require('passport-http').BasicStrategy;
var pg_config_clone = JSON.parse( JSON.stringify( config.pg_config ) );
var requestHandler = require('./lib/routes/jsonapi').requestHandler;
var pwdRecoveryHandler = require('./lib/routes/pwdrecovery').pwdRecoveryHandler;
var i18n = require('i18n-abide');

var app = express();

// all environments
app.set('port', process.env.PORT || config.port);

app.configure('development', function () { app.locals.pretty = true; });

function errorHandler(err, req, res, next) {
    var code = err.code;
    var message = err.message;
    res.writeHead(code, message, {'content-type' : 'text/plain'});
    res.end();//message);
}

app.use(express.logger('dev'));
app.use(express.compress());//all responses are being compressed
app.use(express.favicon());
app.use(express.json({limit:'50mb'}));
app.use(express.urlencoded());
//app.use(express.methodOverride());
//app.use(errorHandler);

app.use('/css',express.static(path.join(__dirname, './client/src/assets/css')));
app.use('/app', express.static(path.join(__dirname, './client/src/app')));
app.use('/images',express.static(path.join(__dirname, './client/src/assets/img')));
app.use('/libs', express.static(path.join(__dirname, './client/vendor')));
//app.use(express.static(path.join(__dirname, '../client/src')));

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
}

app.use(passport.initialize());

passport.use(new BasicStrategy( 
  function(username, password, done) {
    pg_config_clone.user = username;
    pg_config_clone.password = password;

    //console.log(pg_config_clone);
  
    pg.connect(pg_config_clone, function(err, client, pgDone) {
      pgDone();
      if(err) {
        console.log(err);
        return done('Cannot connect', false);
      }
      else
      {
          return done(null, client);
      }
    });
  }
));

app.use(i18n.abide({
  supported_languages: ['en-US', 'fr', 'es', 'db-LB'],
  default_lang: 'en-US',
  debug_lang: 'db-LB',
  translation_directory: 'i18n'
}));

var setDefaultCredentials = function (req, res, next) {
    //console.log('setting default credentials');
    console.log('headers:', req.headers);
    console.log('request data:', req.body);
    if(!req.get('authorization'))
    {
        //console.log('No authorization info sent!');
        req.headers.authorization = 'Basic ' + new Buffer(config.pg_config.user + ':' + config.pg_config.password).toString('base64');
        //console.log(req.headers.authorization);
        //console.log(req.body);
    }
    next();
};

var setCurrentSession = function (req, res, next) {
    if(req.get('Session-Id'))//set current PgSql role
    {
        console.log('Session-Id: ' + req.get('Session-Id'));
        var pgClient = req.user;
        pgClient.query('SELECT * FROM setcurrentuser($1)', [req.get('Session-Id')], function(err, result) {
            var exp_err;
            if(err) {
                //res.send(401, 'Unauthorized, invalid session');
                exp_err = new Error('Unauthorized, invalid session');
                exp_err.code = 401;
                //req.connection.destroy();
                return next(exp_err);
            }
            else
            {
                if(result.rows[0].accepted)
                {
                    if(result.rows[0].new_token)
                        res.setHeader('Session-Id', result.rows[0].new_token);
                    return next();
                }
                else
                {
                    /*res.send(401, 'Unauthorized, invalid session');
                    req.connection.destroy();*/
                    
                    exp_err = new Error('Unauthorized, invalid session');
                    exp_err.code = 401;
                    return next(exp_err);
                }
            }
        });
    }
    else
    {
        console.log('No session-id');
        return next();
    }
};

app.all('/api', setDefaultCredentials, passport.authenticate('basic', { session: false }), setCurrentSession, requestHandler, errorHandler);
app.all('/api/*', setDefaultCredentials, passport.authenticate('basic', { session: false }), setCurrentSession, requestHandler, errorHandler);

//password recovery mechanism
app.all('/recovery', setDefaultCredentials, passport.authenticate('basic', { session: false }), setCurrentSession, pwdRecoveryHandler);

//serve index and view partials
app.get('/', routes.index);
//app.get('/partials/:name', routes.partials);

//redirect all others to the index (HTML5 history)
app.all('*', routes.index);
//app.all('*', express.static(path.join(__dirname, '../client/src')));
//app.all('*', function(error, req, res, next){next('ERROR!!!');});

app.use(app.router);

var options = {key: fs.readFileSync(config.ssl_key), cert: fs.readFileSync(config.ssl_cert)};

https.createServer(options, app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
